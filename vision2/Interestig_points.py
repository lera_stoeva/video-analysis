import numpy
import cv2
import sklearn
#from sklearn.cluster import KMeans


with open("walk-complex-stip.txt") as f:
    file = f.read()
    file = file.split('\n')
f.close()

matrix = []
n = len(file)
for i in range(3, n - 1):
    matrix.append(list(map(numpy.float32, file[i].split('\t')[7:-1])))

matrix = numpy.array(matrix)

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
flags = cv2.KMEANS_RANDOM_CENTERS
compactness,labels,centers = cv2.kmeans(data = matrix, K = 10, bestLabels = None,
                            criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 1, 10), attempts=10,
                            flags=cv2.KMEANS_RANDOM_CENTERS)


import numpy

def find_matrix(A, B, match, deletion, shift):

    matrix = numpy.zeros((len(A) + 1, len(b) + 1), dtype=int)

    for i in range(1, len(A) + 1):
        matrix[i, 0] = deletion * i

    for i in range(1, len(B) + 1):
        matrix[0, i] = deletion * i

    indicator = 0
    for i in range(1, len(A) + 1):
        for j in range(1, len(B) + 1):
            if (A[i - 1] == B[j - 1]):
                indicator = match
            else:
                indicator = shift

            M = max(matrix[i - 1, j] + deletion, matrix[i, j - 1] + deletion)
            M = max(M, matrix[i - 1, j - 1] + indicator)
            matrix[i, j] = M
    return matrix


def Smith(A, B, match, deletion, shift):
    matrix = numpy.zeros((len(A) + 1, len(B) + 1), dtype=int)

    for i in range(1, len(A) + 1):
        matrix[i, 0] = deletion * i

    for i in range(1, len(B) + 1):
        matrix[0, i] = deletion * i

    indicator = 0
    maximum = 0
    for i in range(1, len(A) + 1):
        for j in range(1, len(B) + 1):
            if (a[i - 1] == b[j - 1]):
                indicator = match
            else:
                indicator = shift
            M = max(matrix[i - 1, j] + deletion, matrix[i, j - 1] + deletion)
            M = max(M, matrix[i - 1, j - 1] + indicator)
            M = max(M, 0)
            matrix[i, j] = M
            if M > maximum:
                maximum = M

    answer = []
    for i in range(1, len(a) + 1):
        for j in range(1, len(b) + 1):
            if matrix[i, j] == maximum:
                substringA = []
                substringB = []
                si = i
                sj = j

                while si > 0 and sj > 0:
                    if (a[si - 1] == b[sj - 1]):
                        indicator = match
                    else:
                        indicator = shift


                    if (matrix[si - 1, sj - 1] + indicator == matrix[si, sj]):
                        substringA.append(a[si - 1])
                        substringB.append(b[sj - 1])
                        si -= 1
                        sj -= 1
                    elif matrix[si - 1, sj] + deletion == matrix[si, sj]:
                        substringA.append(a[si - 1])
                        substringB.append('*')
                        si -= 1
                    else:
                        substringB.append(b[sj - 1])
                        substringA.append('*')
                        sj -= 1
                substringA = list(reversed(substringA))
                substringB = list(reversed(substringB))
                answer.append((''.join(substringA), ''.join(substringB)))
    print(matrix)
    print(answer)
    return answer

a = raw_input()
b = raw_input()
Smith(a,b, 1, -1, -1)
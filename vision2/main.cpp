#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cvaux.hpp>
#include <opencv2/core.hpp>

using namespace cv;



int main() {
    namedWindow("input");
    namedWindow("output");
    namedWindow("output1");
    namedWindow("output2");

    Mat src = imread("/Users/valerystoeva/Desktop/bolsh-panda-2.jpg");
    Mat Gaussian{};
    Mat median{};
    Mat dst{};
    
    
    GaussianBlur(src, Gaussian, Size(5, 5), 1.4 , 1.4);
    medianBlur(src, median, 5);
    
    Mat rot_mat( 2, 3, CV_32FC1 );//матрица 2 на 3 из дабл
    
    Point center = Point( src.cols/2, src.rows/2 ); //центр изображения
    
    rot_mat = getRotationMatrix2D(center, 15 , 1); // матрица поворота
    
    warpAffine(Gaussian, dst, rot_mat, Gaussian.size() ); // поворот (использование матрицы, не обязательно поворот)
    
    
    
    imshow("output1", Gaussian);
    imshow("output2", median);
    imshow("output", dst);
    imwrite("/Users/valerystoeva/Desktop/output.jpg", dst);
    waitKey();
    
    
    return 0;
}
